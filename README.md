This is a starter template for [Ionic](http://ionicframework.com/docs/) projects.

## How to use this template

*This template does not work on its own*. The shared files for each starter are found in the [ionic2-app-base repo](https://github.com/ionic-team/ionic2-app-base).

To use this template, either create a new ionic project using the ionic node.js utility, or copy the files from this repository into the [Starter App Base](https://github.com/ionic-team/ionic2-app-base).

### With the Ionic CLI:

```bash
$ npm uninstall -g ionic
$ npm install -g ionic cordova
$ ionic start qrcode_magazzino_mob sidemenu
You can downgrade to your old version by running: $ npm install -g ionic@3.5.0
                                                    npm install --save --save-exact ionic@3.6.0
npm install --save-dev --save-exact ionic@latest

//risolve errore indicato qui https://forum.ionicframework.com/t/typings-error-cannot-find-type-definition-file-for-node-virtual-file-utils-d-ts-line-1/77533/2
$ npm install @types/node --save 

//per gestire la localizzazione delle stringhe
$ npm install @ngx-translate/core @ngx-translate/http-loader --save

//per leggere la versione del client
$ ionic cordova plugin add cordova-plugin-app-version
$ npm install --save @ionic-native/app-version

//plugin qr-scanner
$ ionic cordova plugin add phonegap-plugin-barcodescanner
$ npm install --save @ionic-native/barcode-scanner

TO UNISTALL
$ npm uninstall --save @ionic-native/barcode-scanner
$ ionic cordova plugin remove phonegap-plugin-barcodescanner

//database locale
$ ionic cordova plugin add cordova-sqlite-storage
$ npm install --save @ionic-native/sqlite

//miniumaze app
$ ionic cordova plugin add cordova-plugin-appminimize
$ npm install --save @ionic-native/app-minimize

//native storage
$ ionic cordova plugin add cordova-plugin-nativestorage
$ npm install --save @ionic-native/native-storage

//vibrazione
$ ionic cordova plugin add cordova-plugin-vibration
$ npm install --save @ionic-native/vibration

//componente calendario
$ npm i datepicker-ionic2 --save
```

//per aggiungere view o rpovider
```bash
$ ionic generate page login
% ionic generate provider globalConf
```

Poi , per fare run , cd into `qrcode_magazzino_mob` and run:


```bash
$ ionic serve
$ ionic cordova platform add android
$ \
$ ionic cordova build android
$ ionic cordova build android --prod --release. per ora ha bug uglify. quindi fai:
$ ionic cordova build android --aot --minifycss --optimizejs --release
```

``` Generare icona e splashscreen
1. 1024x1024px and located at resources/icon.png.
2. 2732x2732px and located at resources/splash.png
$ ionic cordova resources
```

```DEBUG
chrome://inspect/#devices
```

CLI instal update:

 The Ionic CLI can automatically check for CLI updates in the background. Would you like to enable this? Yes
? Local plugin @ionic/cli-plugin-cordova has an update available (1.4.1 => 1.5.0)! Would you like to install it? Yes
> npm install --save-dev --save-exact @ionic/cli-plugin-cordova@latest
√ Running command - done!
[OK] Updated @ionic/cli-plugin-cordova to 1.5.0! �
? Local plugin @ionic/cli-plugin-ionic-angular has an update available (1.3.2 => 1.4.0)! Would you like to install it? Yes
> npm install --save-dev --save-exact @ionic/cli-plugin-ionic-angular@latest
√ Running command - done!
[OK] Updated @ionic/cli-plugin-ionic-angular to 1.4.0! �
> npm dedupe
√ Running command - done!

DEV INFO:

url postman:
http://qrcode.treagles.it/api/V0.1/config?username=walker&password=walker


