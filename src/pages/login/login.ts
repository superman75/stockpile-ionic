import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {User} from "../../model/user";
import {GlobalConfProvider} from "../../providers/global-conf/global-conf";
import {AuthManagerProvider} from "../../providers/auth-manager/auth-manager";
import {DataManagerProvider} from "../../providers/data-manager/data-manager";
import {TranslateService} from "@ngx-translate/core";
import {SimpleAlertProvider} from "../../providers/simple-alert/simple-alert";
import { NativeStorage } from '@ionic-native/native-storage';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AngularFireAuth } from 'angularfire2/auth';
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import * as firebase from 'firebase';
import {AngularFireDatabase} from 'angularfire2/database';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  //
  // C_USER_LOGGED:string="USERLOGGED";
  // currentUser:User;
  // loadingPanel;
  //
  // constructor(public navCtrl: NavController,
  //             public navParams: NavParams,
  //             public globalConf:GlobalConfProvider,
  //             public loadingCtrl: LoadingController,
  //             public authManager:AuthManagerProvider,
  //             public dataManager:DataManagerProvider,
  //             public translate: TranslateService,
  //             public alertCtrl:AlertController,
  //             public simpleAlert:SimpleAlertProvider,
  //             private nativeStorage: NativeStorage,
  //             public menu: MenuController) {
  //
  //   //se vengo da Logout non devo fare login automatica
  //   let fromLogout = false;
  //   fromLogout = navParams.get('fromLogout');
  //
  //   //leggo credenziali e popolo oggetto currentUser
  //   this.currentUser = new User("","");
  //   this.nativeStorage.getItem(this.C_USER_LOGGED)
  //     .then(
  //       data => {
  //         console.log(data)
  //         this.currentUser = new User(data.username,data.password);
  //         //se trovo utente eseguo login automatico, se naturalmente non vengo da logout
  //          if(!fromLogout){
  //            this.login();
  //          }
  //       },
  //           error => console.error(error)
  //       );
  //   /*
  //   dataManager.readUser().then((res) => {
  //     let username = this.globalConf.userLogged.username;
  //     let password = this.globalConf.userLogged.password;
  //     this.currentUser = new User(username,password);
  //   });*/
  //
  //  // this.currentUser = new User("walker","walker");
  // }
  //
  // login() {
  //   //eseguo check semplici
  //   if (this.currentUser.username==null || this.currentUser.password==null){
  //     alert(this.translate.instant('LOGIN.ALERT_LOGIN_CREDENTIAL_EMPTY'));
  //   }else if (this.currentUser.username.length==0 || this.currentUser.password.length==0){
  //     alert(this.translate.instant('LOGIN.ALERT_LOGIN_CREDENTIAL_EMPTY'));
  //   } else {
  //     this.doLogin();
  //   }
  // }
  //
  // private doLogin(){
  //   //Effettuo chiamate api. apro pannello di attesa
  //   this.loadingPanel = this.loadingCtrl.create({
  //     content: this.translate.instant('Attendi login')
  //   });
  //   this.loadingPanel.present();
  //   //chiamo api
  //   this.authManager.login(this.currentUser.username, this.currentUser.password).then((data:any) => {
  //     if (data._body != null) {
  //       //check sul tipo stringa. in caso di errore di rete _body Ã¨ un ogetto complesso
  //       if (typeof data._body === 'string') {
  //         let serverResponse = data._body.trim().toUpperCase();
  //         if (serverResponse == "OK") {
  //           //OK LOGGATO
  //           //salvo credenziali per accesso successivo
  //           this.globalConf.userLogged =new User(this.currentUser.username,this.currentUser.password);
  //
  //           this.nativeStorage.setItem(this.C_USER_LOGGED, {username: this.currentUser.username, password: this.currentUser.password})
  //             .then(
  //               () => console.log('Stored item!'),
  //               error => console.error('Error storing item', error)
  //             );
  //           //this.dataManager.saveUser(this.currentUser.username, this.currentUser.password);
  //
  //           this.loadingPanel.data.content = this.translate.instant('LOGIN.USER_AUTH_OK') + ". " +this.translate.instant('SYNC.ALERT_WAIT_DOWNLOAD');
  //           //carico conf. non chiudo form di attesa
  //           this.loadServerConfig();
  //           this.globalConf.setCurrentePage("Home");
  //           this.navCtrl.setRoot(HomePage);
  //         } else if(serverResponse.length ==0) {
  //           //se body Ã¨ nullo vedo el var status e statuText, potrebbe essere errore server tipo 500x o 400x
  //           try{
  //             if (data.status !="" || data.statusText!=""){
  //               alert(this.translate.instant('LOGIN.SERVER_NOT_REACH') + " " + data.status + " - " + data.statusText);
  //               this.loadingPanel.dismiss();
  //             }else{
  //               alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
  //               this.loadingPanel.dismiss();
  //             }
  //           } catch (ex){
  //             alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
  //             this.loadingPanel.dismiss();
  //           }
  //           //se il msg non Ã¨ tanto lungo lo mostro subito all'utente
  //         } else if (serverResponse.length < 30) {
  //           alert(this.translate.instant('LOGIN.ALERT_LOGINERROR') + ": " + serverResponse);
  //           this.loadingPanel.dismiss();
  //         }else{
  //           this.loadingPanel.dismiss();
  //           //messaggio lungo o metto nel dettaglio
  //           this.showDetailedErrorLogin(serverResponse);
  //         }
  //       } else {
  //         //vedo se status Ã¨ valorizzato, potrebbe essere er 500x o 400x anche se lo dovrebbe beccare sopra
  //         if (data.status !=null && data.statusText!=null){
  //           if (data.status !="" || data.statusText!=""){
  //             alert(this.translate.instant('LOGIN.SERVER_NOT_REACH') + " " + data.status + " - " + data.statusText);
  //             this.loadingPanel.dismiss();
  //           }else{
  //             //tutti gli altri casi Ã¨ rete non disponbibile
  //             alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
  //             this.loadingPanel.dismiss();
  //           }
  //         }else{
  //           //tutti gli altri casi Ã¨ rete non disponbibile
  //           alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
  //           this.loadingPanel.dismiss();
  //         }
  //       }
  //     } else {
  //       //tutti gli altri casi Ã¨ rete non disponbibile
  //       alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
  //       this.loadingPanel.dismiss();
  //     }
  //   });
  // }
  //
  // //dopo la login recupera la lista dei prodotti
  // private loadServerConfig(){
  //   this.loadingPanel.data.content = this.translate.instant('LOGIN.USER_AUTH_OK') + ". " +this.translate.instant('LOGIN.ALERT_WAIT_DOWNLOAD');
  //   this.dataManager.loadServerConfig().then((res) => {
  //     this.loadingPanel.dismiss();
  //     let alert;
  //     if (res=='end'){
  //       //TUTTO OK. non mostro conferma
  //       //alert = this.simpleAlert.createAlert(this.translate.instant('LOGIN.ALERT_CONF_OK'), this.translate.instant('LOGIN.ALERT_CONF_MSG_OK'));
  //     }else{
  //       alert = this.simpleAlert.createAlert(this.translate.instant('LOGIN.ALERT_CONF_NOT_READ'), this.translate.instant('LOGIN.ALERT_CONF_NOT_READ_MSG'));
  //       setTimeout(() => {
  //         alert.present();
  //       }, 500);
  //     }
  //     this.navCtrl.setRoot(HomePage);
  //
  //   });
  // }
  //
  // //mostra un popup per vedere il dettaglio errore
  // private showDetailedErrorLogin(respServer:string){
  //   let confirm = this.alertCtrl.create({
  //     title: this.translate.instant('LOGIN.SERVER_NOT_REACH'),
  //     message: this.translate.instant('LOGIN.PRESS_DETAILS'),
  //     buttons: [
  //       {
  //         text: this.translate.instant('GLOBAL.NO'),
  //         handler: () => {
  //         }
  //       },
  //       {
  //         text: this.translate.instant('LOGIN.DETAILS'),
  //         handler: () => {
  //           alert(respServer);
  //         }
  //       }
  //     ]
  //   });
  //   confirm.present();
  // }
  //
  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad LoginPage');
  // }
  //
  // //Side menù disabiliato su Login
  // ionViewWillEnter() {
  //   this.menu.swipeEnable(false)
  // }
  //
  // ionViewWillLeave() {
  //   this.menu.swipeEnable(true)
  // }
    loginForm: FormGroup;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private fb: FormBuilder,
        private loadingCtrl: LoadingController,
        public translate: TranslateService,
        private toastCtrl: ToastController,
        private fireAuth: AngularFireAuth,
        private afDB : AngularFireDatabase) {
        this.loginForm = this.buildLoginForm();
    }

    ionViewDidLoad() {

    }

    private buildLoginForm() {
        return this.fb.group({
            // 'email': ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
            // 'password': ['', Validators.required]
            email: ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]

        });
    }

    login(){
        let loading = this.loadingCtrl.create({
            content: this.translate.instant('LOGIN.Attendi_login'),
            dismissOnPageChange: true
        });

        loading.present();

        let email = this.loginForm.get('email').value;
        let password = this.loginForm.get('password').value;

        this.fireAuth.auth.signInWithEmailAndPassword(email, password)
            .then((res) => {
                loading.dismiss();
                let user = firebase.auth().currentUser;
                if (user.emailVerified){
                    this.fireAuth.authState.take(1).subscribe( data=>{
                        this.afDB.object('/users/'+data.uid+'/userId').set(data.uid);
                        this.afDB.object('/users/'+data.uid+'/email').set(data.email);
                        this.afDB.object('/users/'+data.uid+'/roleId').set("3");
                        this.afDB.object('/users/'+data.uid+'/numofproducts').set(0);

                    });
                    this.navCtrl.setRoot(HomePage);
                } else {
                    this.toastCtrl.create({
                        message: this.translate.instant('LOGIN.ALERT_NON_VERIFY'),
                        duration: 2000
                    }).present();
                }
            })
            .catch(err =>{
                console.log(err);
                loading.dismiss();
                this.toastCtrl.create({
                    message: this.translate.instant('LOGIN.ALERT_LOGINERROR'),
                    duration: 2000
                }).present();
            })
    }
    goToRegisterPage(){
        this.navCtrl.push(RegisterPage);
    }

}
