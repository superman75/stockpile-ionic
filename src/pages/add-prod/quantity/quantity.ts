import {Component, ViewChild} from '@angular/core';
import {ViewController, Content, Platform} from 'ionic-angular';

/**
 * Generated class for the AddProdPage page.
 *
 * Form per inserimento nuovo prodotto
 */

declare var cordova;
declare var window;
@Component({
  selector: 'quantity',
  templateUrl: 'quantity.html',
})
export class Quantity {
  @ViewChild('quantitytag') MyinputQuantity;
  @ViewChild(Content) content: Content;
  amount;
  candismiss:boolean = true;
  constructor(private view: ViewController,
              private plt: Platform) {
    // metto ritardo altrimenti this.MyinputQuantity è null
    setTimeout(()=>{ this.setFocusNumItems()
    },500);
  }


  setFocusNumItems(){
    if(this.plt.is('cordova') &&  this.MyinputQuantity!=null) {
      //metto focus direttamente su numitnes
      this.MyinputQuantity.setFocus();
      window.cordova.plugins.Keyboard.show();
    }
  }

  /**
   * alla pressione del tasto enter dismetto la form. su samsung non serviva su huwawei si
   * @param keycode
     */
  enterEv(keycode){
   if (keycode==13) {
       this.view.dismiss(this.amount);
   }
  }


  /**
   * se cambio focus chiudo la modal e passo il valore al papà
   */
  amountChangeEv(){
    if (this.candismiss) {
      this.candismiss = true;
      this.view.dismiss(this.amount);
    }
  }
}
