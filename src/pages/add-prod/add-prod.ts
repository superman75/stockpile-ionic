import {Component, ViewChild} from '@angular/core';
import {
  NavController, NavParams, Platform, AlertController, LoadingController, ModalController,
  ToastController
} from 'ionic-angular';

import { DataManagerProvider } from "../../providers/data-manager/data-manager";
import { Product } from "../../model/product";
import { TranslateService } from "@ngx-translate/core";
import {Category} from "../../model/category";
import {Type} from "../../model/type";
import {GlobalConfProvider} from "../../providers/global-conf/global-conf";
import {Vibration} from "@ionic-native/vibration";
import {Warehouse} from "../../model/warehouse";
import {HomePage} from "../home/home";
import {QRScannerPage} from "../qrscanner/qrscanner";
import {SmartAudio} from "../../providers/smart-audio/smart-audio";
import {QRScanner} from "@ionic-native/qr-scanner";
import {DatePickerDirective} from "datepicker-ionic2/index";
import {Quantity} from "./quantity/quantity";
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase} from "angularfire2/database";

/**
 * Generated class for the AddProdPage page.
 *
 * Form per inserimento nuovo prodotto
 */

@Component({
  selector: 'page-add-prod',
  templateUrl: 'add-prod.html',
})
export class AddProdPage {

  @ViewChild ('mycalendar') MyCalendar;
  @ViewChild(DatePickerDirective) public datepicker: DatePickerDirective;
  //pannellino di attesa inserimento
  loadingPanel:any;
  defaultamount = "__";


  //proddotto bindato alla form
  currentProduct:any;
  products : any = [];
  warehouses : Warehouse[] = [];
  types : any = [];
  categories : any = [];
  userId : any;
  max : number;
  //se chiamata dalla ricerca cioè da list-prod quando trovo match
  fromSearch:boolean;

  //data tornata dal controllo calendario
  bestbeforedate;

  //ausiliario per controllo datetime
  public localDate: Date = new Date();
  public bestbeforedateDATE: Date = new Date();
  public disabledDates: Date[] = [new Date(2017, 7, 14)];
  public maxDate: Date = new Date(new Date().setDate(new Date().getDate() + 3000));
  public min: Date = new Date();
  public cancelText="Cancella";
  public localeStrings ={ monday:true, weekdays: ['Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab', 'Dom'],
    months: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'] };

  //booleani
  //indica se bottone salva è disabilitato o meno
  disabledSave=true;

  //QR Code Risultato
  results:any;

  //lista dei tipi filtrati sulla categoria. bindato alla form
  l_typeListFiltered:Array<Category>=[];

  private bestbeforedateDATEEdited:boolean = false;
  public titleAddOrEdit:string;
  public isAdd:boolean = true;
  mycategory_id:string;//category da bindare

  public selectedWarehouse:any;
  public selectedCategory:any;
  public selectedType:any;

  public html_warehouse_label:string = this.translate.instant('ADDPROD.MAGAZZINO');
  public icon_warehouse:string = "basket";
  public html_category_label:string = this.translate.instant('ADDPROD.CATEGORIA');
  public icon_category:string = "clipboard";
  public html_type_label:string = this.translate.instant('ADDPROD.TIPO');
  public icon_type:string = "pricetag";

  public originalProduct:Product = null; //è l'istanza del capo, originale passata dal padre

  //array configurazione
 /* stores:Array<{key: number, value: string}>=[{key: 0, value: "magazzino0"}, {key: 1, value: "magazzino1"},{key: 2, value: "magazzino2"}];
  types:Array<{key: number, value: string}>=[{key: 0, value: "tipo0"}, {key: 1, value: "tipo1"},{key: 2, value: "tipo2"}];
  categories:Array<{key: number, value: string}>=[{key: 0, value: "categoria0"}, {key: 1, value: "categoria1"},{key: 2, value: "categoria2"},{key: 3, value: "categoria3"},{key: 4, value: "categoria4"}];
*/

    /**
     * costruttore
     * @param navCtrl
     * @param navParams
     * @param platform
     * @param alertCtrl
     * @param loadingCtrl
     * @param translate
     * @param dataManager
     * @param globalConf
     * @param vibration
     * @param modalCtrl
     * @param toastCtrl
     * @param smartAudio
     * @param qrScanner
     * @param afDB
     * @param afAuth
     */
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private platform: Platform,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              public translate: TranslateService,
              private dataManager:DataManagerProvider,
              private globalConf:GlobalConfProvider,
              private vibration: Vibration,
              public modalCtrl: ModalController,
              public toastCtrl: ToastController,
              public smartAudio: SmartAudio,
              public qrScanner: QRScanner,
              private afDB : AngularFireDatabase,
              private afAuth : AngularFireAuth) {
    afAuth.authState.take(1).subscribe( data=>{
        this.userId = data.uid;
        this.products = this.afDB.list('/products',
            ref=>ref.orderByChild('user_id').equalTo(this.userId)
        ).valueChanges();

    });
    this.afDB.list('warehouses').valueChanges()
        .subscribe(snapshots=>{
          snapshots.forEach(snapshot=>{
            let warehouse = new Warehouse(snapshot['id'],snapshot['name']);
            this.warehouses.push(warehouse);
          })
        });
    this.afDB.list('types').valueChanges()
        .subscribe(snapshots=>{
            snapshots.forEach(snapshot=>{
                let type = new Type(snapshot['id'],snapshot['name'],snapshot['category_id']);
                this.types.push(type);
            })
        });
    this.afDB.list('categories').valueChanges()
        .subscribe(snapshots=>{
            snapshots.forEach(snapshot=>{
                let category = new Category(snapshot['id'],snapshot['name']);
                this.categories.push(category);
            })
        });

    this.clearAllField();
    let cur_prod_id  = navParams.get('prodSelected');

    this.fromSearch = navParams.get('fromSearch');

    //vado in edit
    if ( cur_prod_id != null) {
      if ( this.fromSearch){
          this.showAlertProductEditPresent(this.translate.instant('ADDPROD.QRCODE_MATCH_SEARCH_OK'));
      }
      this.afDB.object('/products/'+cur_prod_id).valueChanges()
        .subscribe(value=>{
            this.currentProduct = value;
            this.bestbeforedate = this.currentProduct.bestbeforedate;
            this.bestbeforedateDATE = this.globalConf.dateconvert_from_yyyy_mm_dd_hh_mm_ss_toDATE(this.bestbeforedate);
            this.defaultamount = "";
            this.isAdd = false;
            this.titleAddOrEdit = this.translate.instant('ADDPROD.TITLE_EDIT') + " " + this.currentProduct.local_id;
            this.afDB.object('/warehouses/'+this.currentProduct.warehouse_id).valueChanges()
                .subscribe(value=>{
                    this.selectedWarehouse = new Warehouse(value['id'],value['name']);
                });
            this.afDB.object('/types/'+this.currentProduct.type_id).valueChanges()
                .subscribe(value=>{
                    this.selectedType = new Type(value['id'],value['name'],value['category_id']);
                });
            this.getCategory();
        });
    }
  }
  ionViewDidLoad(){
      this.afDB.object('users/'+this.userId).valueChanges()
          .subscribe(value=>{
              this.max = value['numofproducts'];
          })
  }
  getCategory(){
    console.log(this.currentProduct.type_id);
    this.afDB.object('types/'+this.currentProduct.type_id).valueChanges()
        .subscribe(value => {
            this.mycategory_id = value['category_id'];
            this.populateFilterTypes(this.mycategory_id);
            this.DoCopyInOriginalPutCurrent();
            this.afDB.object('/categories/'+this.mycategory_id).valueChanges()
                .subscribe(value=>{
                    this.selectedCategory = new Category(value['id'],value['name']);
                });
        });
  }

  /**
   * check a posteriori sulla formattazione del numero, non può contenere più di un separatore, irgola o punto che sia
   * @param amount
   * @returns {boolean}
     */
  private checkCorrectFieldAmount(amount:string){
    //conto
    if (amount==null){
      return false;
    }

    let countcommas = amount.split(",").length-1;
    let countpoints = amount.split(".").length-1;
    let countSeparator = countcommas + countpoints;
    if (countSeparator > 1 || amount==""){
      return false
    }
    return true;
  }


  public productEdited(){
      //cambio il titolo, per dare una info in più all'utente. metto * se cambio.
      //A livello di codice lo tolgo con split e poi join con vuoto e lo rimetto se serve
      if (!this.isAdd) {
        this.disabledSave = false;
      }
  }

  /**
   * Dato il type id recupera il Type con quell'id dallalista dei filtrati
   * @param typeid
   * @returns {Type}
     */
  private getTypeFromListFiltered(typeid):Type{
    for (let i = 0; i < this.l_typeListFiltered.length; i++) {
      let t:Type = this.l_typeListFiltered[i];
      if (t.id == typeid){
        return t;
      }
    }
  }

  /**
   * evento scatenato sul cambio di categoria. Filtra i tipi
   * @param e
     */
  onCategorySelect(e){
    let cat_id = e;
    this.populateFilterTypes(cat_id);
    //reset del type id
    this.currentProduct.type_id = null;
    this.productEdited();
  }

  /**
   * filtra i tipi
   * @param cat_id
     */
  private populateFilterTypes(cat_id){
    this.l_typeListFiltered = [];
    //filtro la lista dei tipi a seocnda della categoria selezionata
    for (let i = 0; i < this.dataManager.m_typeList.length; i++) {
      let type = this.dataManager.m_typeList[i];
      if (type.category_id == cat_id){
        this.l_typeListFiltered.push(new Type(type.id,type.name,type.category_id));
      }
    }
  }

  /**
   * Funzione di callback dopo la scelta della data sul calendario
   * @param date
     */
  setDate(date: Date) {
    console.log(date);
    this.bestbeforedateDATEEdited = true;
    this.bestbeforedateDATE = date;
    this.bestbeforedate = this.globalConf.dateconvert_to_yyyy_mm_dd_hh_mm_ss(date);
    this.currentProduct.bestbeforedateDATE = this.bestbeforedateDATE;
    this.currentProduct.bestbeforedate = this.bestbeforedate;
    this.productEdited();
    //in edit modifico
    if (!this.isAdd){

    }
  }

  /**
   * Chiama api web per l'inserimento nuovo prodotto/ edit prodotto su db
   */
  addOrEditProduct(){
    let productToInsert:Product;
    let msgWait:string;

    //check sulla correttezza del campo quantità
    if (!this.checkCorrectFieldAmount(this.currentProduct.amount)){
      alert(this.translate.instant('ADDPROD.ALERT_AMOUNT_ERR'));
      return;
    }
    // this.currentProduct.qrcode = 200;

    if (this.isAdd){
      msgWait = this.translate.instant('ADDPROD.ALERT_WAIT_INSERT');
      //se add metto un nuovolocal_id
      let today = new Date();
      productToInsert = new Product(this.currentProduct.id,(this.max+1).toString(), this.currentProduct.warehouse_id,
          this.currentProduct.type_id,this.currentProduct.qrcode, this.currentProduct.amount,
          this.bestbeforedate,this.globalConf.dateconvert_to_yyyy_mm_dd_hh_mm_ss(today));
      //check sui campi
    }else{
      msgWait = this.translate.instant('ADDPROD.ALERT_WAIT_UPDATE');
      //aggiorno il campo data
      this.currentProduct.bestbeforedate =  this.bestbeforedate;
    }


    //Effettuo chiamate api. apro pannello di attesa
    this.loadingPanel = this.loadingCtrl.create({
      content: msgWait
    });
    this.loadingPanel.present();

    //edit o add
    if (this.isAdd){
      this.addProd(productToInsert);
      alert("This item is added successfully");
    }else{
      this.editProd();
      alert("This item is updated successfully");
    }
    this.disabledSave = true;
  }

  /**
   * Edit prodotto a db
   * @param productToInsert
     */
  private editProd(){

    console.log('checking..');
    //controllo sui campi nulli
    if (this.currentProduct.warehouse_id==null ||
        this.currentProduct.type_id==null ||
        this.currentProduct.amount==null ||
        this.currentProduct.qrcode==null){
      this.loadingPanel.dismiss();
      alert(this.translate.instant('ADDPROD.ALL_FIELD'));
      return;
    }
    let isExpired;
    if(this.currentProduct.bestbeforedate>this.globalConf.dateconvert_to_yyyy_mm_dd_hh_mm_ss(new Date())){
        isExpired = false;
    } else isExpired = true;
    this.afDB.object('/products/'+this.currentProduct.id).update({
        amount : this.currentProduct.amount,
        id : this.currentProduct.id,
        bestbeforedate : this.currentProduct.bestbeforedate,
        insertdate : this.currentProduct.insertdate,
        warehouse_id : this.currentProduct.warehouse_id,
        type_id : this.currentProduct.type_id,
        isExpired : isExpired
    });
    this.afDB.object('/types/'+this.selectedType.id).update({
        category_id : this.selectedType.category_id
    });
    this.loadingPanel.dismiss();
    // this.dataManager.updateProduct(productToUpdate).then((data:any) => {
    //   //test sulla risposta
    //   if (data._body!=null){
    //     try {
    //       let myres = JSON.parse(data._body);
    //       if (myres.local_id==productToUpdate.local_id){
    //         this.dataManager.sortProductsOnDate();//ordino nuovamewnte l'array poichè ho appena inserito un prodotto
    //         this.loadingPanel.dismiss();
    //         this.disabledSave=true; //disabilito il booleano così evito check, visto che poi esco e torno sulla lista
    //         this.navCtrl.pop();
    //         //Ho tolto la conferma.come richeista cliente
    //         //that.showAlertProductAdded(this.translate.instant('ADDPROD.ALERT_PRODEDITED_MSG'));
    //       }
    //     } catch(e) {
    //       //vedo se status è valorizzato, potrebbe essere er 500x o 400x anche se lo dovrebbe beccare sopra
    //       if (data.status !=null && data.statusText!=null){
    //         if (data.status !="" || data.statusText!=""){
    //           this.loadingPanel.dismiss();
    //           alert(this.translate.instant('LOGIN.SERVER_NOT_REACH') + " " + data.status + " - " + data.statusText + " " +  data._body);
    //         }else{
    //           this.loadingPanel.dismiss();
    //           //tutti gli altri casi è rete non disponbibile
    //           alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
    //         }
    //       }else{
    //         this.loadingPanel.dismiss();
    //         //tutti gli altri casi è rete non disponbibile
    //         alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
    //       }
    //     }
    //   }else{
    //     this.loadingPanel.dismiss();
    //     alert(this.translate.instant(JSON.stringify(data)));
    //   }
    //
    // });
  }

  /**
   * Aggiunge nuovo prodotto a db
   * @param productToInsert
     */
  private addProd(productToInsert:Product){

    //controllo sui campi nulli
    if (productToInsert.warehouse_id==null||
      productToInsert.type_id==null||
      productToInsert.amount==null ||
      (productToInsert.qrcode==null ||
          productToInsert.qrcode=="")
    ){
      this.loadingPanel.dismiss();
      alert(this.translate.instant('ADDPROD.ALL_FIELD'));
      return;
    }
    this.loadingPanel.dismiss();
    const id  = this.afDB.list('/products/').push(productToInsert).key;
    this.afDB.object('/products/'+id).update({
        user_id : this.userId,
        id : id,
        isExpired : productToInsert.insertdate>productToInsert.bestbeforedate
    });
    this.afDB.object('/users/'+this.userId).update({
        numofproducts : this.max+1
    })
    //
    //
    // this.dataManager.insertProduct(productToInsert).then((data:any) => {
    //   //test sulla risposta
    //   if (data._body!=null){
    //     try {
    //       let myres = JSON.parse(data._body);
    //       if (myres.local_id==productToInsert.local_id){
    //         //setto l'id del server
    //         productToInsert.id = myres.id.toString();
    //         this.dataManager.m_productList.push(productToInsert);
    //         this.dataManager.sortProductsOnDate();//ordino nuovamewnte l'array poichè ho appena inserito un prodotto
    //         this.loadingPanel.dismiss();
    //         this.navCtrl.setRoot(HomePage); //nuova gestione, dopo la insert del prod va sulla homepage. nessuna conferma
    //        // that.showAlertProductAdded(this.translate.instant('ADDPROD.ALERT_PRODADDED_MSG'));
    //        // that.clearAllField();
    //       }else{
    //         this.loadingPanel.dismiss();
    //         //tutti gli altri casi è rete non disponbibile
    //         alert("Errore durante l/'inserimento. Prova ad uscire e rientrare");
    //       }
    //     } catch(e) {
    //       //vedo se status è valorizzato, potrebbe essere er 500x o 400x anche se lo dovrebbe beccare sopra
    //       if (data.status !=null && data.statusText!=null){
    //         if (data.status !="" || data.statusText!=""){
    //           this.loadingPanel.dismiss();
    //           alert(this.translate.instant('LOGIN.SERVER_NOT_REACH') + " " + data.status + " - " + data.statusText + " " +  data._body);
    //         }else{
    //           this.loadingPanel.dismiss();
    //           //tutti gli altri casi è rete non disponbibile
    //           alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
    //         }
    //       }else{
    //         this.loadingPanel.dismiss();
    //         //tutti gli altri casi è rete non disponbibile
    //         alert(this.translate.instant('LOGIN.ALERT_ERRCONN'));
    //       }
    //     }
    //   }else{
    //     this.loadingPanel.dismiss();
    //     alert(this.translate.instant(JSON.stringify(data)));
    //   }

    // });
  }

  /**
   * dopo add sbianchetto tutti i campi
   */
  private clearAllField(){
    let mydate = new Date();
    this.bestbeforedateDATE = mydate;
    //setto la data scadenza di default ad oggi
    this.bestbeforedate = this.globalConf.dateconvert_to_yyyy_mm_dd_hh_mm_ss(mydate);
    //setto titolo corretto
    this.titleAddOrEdit = this.translate.instant('ADDPROD.TITLE');
    //booleano che memorizza il fatto sono in add
    this.isAdd = true;
    //inizializzo nuovo prodotto bindato alla form
    this.currentProduct = new Product("",
      "",
      null,
      null,
      null,
      null,
      this.bestbeforedate,
      null);

    //sbiancheto categoria
    this.mycategory_id=null;
  }

  /**
   * legge il qrcode. chiamata sul bottone. valorizza il campo qrcode del prodotto correnteche deve essere aggiunto.
   * Esegue check se proddotto qrcode già inserito, manda msg di errore e non binda nulla
   */
  public takeQrCode(){
    if(this.platform.is('cordova')){
      let modal = this.modalCtrl.create(QRScannerPage);
      //devo mettere la trasparenza alla apgina ospitante, altimenti la camera sta sotto
      window.document.querySelector('page-add-prod').classList.add('transparent-body');
      modal.onDidDismiss((qrcode) => {
        //ritolgo la trasparenza appena va bene
        window.document.querySelector('page-add-prod').classList.remove('transparent-body');
        if (this.isAdd) {
          //spengo l'eventuale luce accesa
          this.qrScanner.disableLight();
          let p;
          this.products.subscribe(snapShots=>{
              snapShots.forEach(snapShot=>{
                  if(snapShot.qrcode==qrcode){
                      p = snapShot.id;
                  }
              });
              if(p!=null){
                  //sbianchetto e mostro errore
                  this.currentProduct.qrcode ="";
                  this.showAlertProductAlreadyPresent();
              }else{
                  //ok effetto il bind
                  this.currentProduct.qrcode = qrcode;
                  this.checkCalendarOpened();
              }
          });
          //in EDIT
        }else {
          //se torno indietro vado sulla lista dei prodotti
          if (qrcode==null){
            //spengo l'eventuale luce accesa
            this.qrScanner.disableLight();
            this.navCtrl.pop();
          }else{
            //controllo in edit se stesso qrcode. Non posso modificare qrcode già inserito
            let qrcodeCurrentProd = this.currentProduct.qrcode;
            if(qrcodeCurrentProd==qrcode){
              //spengo l'eventuale luce accesa
              this.qrScanner.disableLight();
              this.showAlertProductEditPresent(this.translate.instant('ADDPROD.QRCODE_MATCH_OK'));
            }else{
              //faccio vibrare e non mando msg di prodotto non trovato
              // Vibrate the device for a second
              // Duration is ignored on iOS.
              this.vibration.vibrate(1000);
              this.takeQrCode();//rilancio la ricerca
            }
          }
        }
      });
      modal.present();
    }else{
      //solo per debug su browser
      this.currentProduct.qrcode = 'debug';
    }
  }

  /**
   * se la data non èsettata e sto in add allora apro automaticamente il calendario per la scelta della data di scadenza
   */
  private checkCalendarOpened(){
   if (!this.bestbeforedateDATEEdited && this.isAdd){
     this.datepicker.open();
   }
  }

  /**
   * mostra alert di prodotto con lo stesso qrcode presente nel magazzino
   */
  showAlertProductAlreadyPresent(){
    //metto suono di insuccesso
    this.smartAudio.play('SameQrCode');
    //messaggio che si autonasconde dopo 2 secondi
    let toast = this.toastCtrl.create({
      message: this.translate.instant('ADDPROD.QRCODE_USED_MSG'),
      duration: 3000,
      position:'top'
    });
    toast.present();
  }

  /**
   * mostra alert di prodotto con lo stesso qrcode presente nel magazzino
   */
  showAlertProductEditPresent(msg:string){
    //metto suono di successo
    this.smartAudio.play('QrcodeSuccess');
    //messaggio che si autonasconde dopo 2 secondi
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position:'top'
    });
    toast.present();
  }

  /**
   * DEPRECATA: mostra alert di prodotto con lo stesso qrcode presente nel magazzino
   */
  showAlertProductEditNotPresent(){
    let alert = this.alertCtrl.create({
      title: 'Qrcode differente',
      subTitle: 'Il qrcode scansionato NON corrisponde a quello del prodotto nel magazzino',
      buttons: ['OK']
    });
    alert.present();
  }

  /**
   * evento callback magazzino selezionato. bind sull'oggetto
   */
  onMyTreaglesComboWarehouseClick(ev){
    this.currentProduct.warehouse_id =ev.selectedItem.id;
    this.productEdited();
    //this.selectedWarehouse = ev.selectedItem;
  }

  onMyTreaglesComboCategoryClick(ev){
    let cat_id = ev.selectedItem.id;
    //filtro i tipoi, che dipendono dalla categoria
    this.populateFilterTypes(cat_id);
    //reset del type id
    this.currentProduct.category_id = ev.selectedItem.id;
    //sbianchetto il tipo
    this.selectedType = new Type(null,null,null);
    this.productEdited();
  }

  onMyTreaglesComboTypeClick(ev){
    //bind del valore
    this.currentProduct.type_id = ev.selectedItem.id;
    this.productEdited();
  }

  /**
   * evento cambio amount
   * @param ev
     */
  amountChangeEv(ev){
    this.productEdited();
  }

  /**
   * Elimina il prodotto e torna sulla lista
   */
  scaricaProduct(){
    let confirm = this.alertCtrl.create({
      title: this.translate.instant('LISTPROD.ALERT_DELETE_PROD'),
      message: this.translate.instant('LISTPROD.ALERT_DELETE_PROD_MSG'),
      buttons: [
        {
          text: 'No',
          handler: () => {
            //nothing
          }
        },
        {
          text: this.translate.instant('LISTPROD.ALERT_DELETE_PROD'),
          handler: () => {
            this.disabledSave = true; //devo solo cancellare quindi non serve check su edit e messagggio di rimanere o no
              this.afDB.object('/products/'+this.currentProduct.id).remove();
          }
        }
      ]
    });
    confirm.present();
  }

  /**
   * Torna semplicemente indietro
   */
  backToList(){
    //esco se non ho toccato nulla
    if (this.disabledSave){
      this.navCtrl.pop();
      //this.navCtrl.push(ListProdPage,{search:false,productToDelete:null});
      return;
    }
    let confirm = this.alertCtrl.create({
      title: this.translate.instant('ADDPROD.ALERT_WARENOTSAVED'),
      message:this.translate.instant('ADDPROD.ALERT_WARENOTSAVED_MSG'),
      enableBackdropDismiss:false,
      buttons: [{
        text: this.translate.instant('ADDPROD.EXIT'),
        handler: () => {
          this.DoCopyInCurrentPutOriginal();
          this.navCtrl.pop();
          //this.navCtrl.push(ListProdPage,{search:false,productToDelete:null});
        },
      }, {
        text: this.translate.instant('GLOBAL.REMAIN'),
        handler: () => {

        }
      }],
    });
    confirm.present();
  }

  //evento lanciato quando si lascia la pagina. Viene fatto il chgeck se ci sono prm modificati.
  //Si chiede conferma
  ionViewCanLeave(): Promise<any> {
    return new Promise((resolve, reject) => {

      if (this.isAdd){
        resolve();
        return;
      }

      //esco se non ho toccato nulla
      if (this.disabledSave){
        //this.navCtrl.pop();//setRoot(ListProdPage,{search:false,productToDelete:null});
        resolve();
        return;
      }

      let confirm = this.alertCtrl.create({
        title: this.translate.instant('ADDPROD.ALERT_WARENOTSAVED'),
        message:this.translate.instant('ADDPROD.ALERT_WARENOTSAVED_MSG'),
        enableBackdropDismiss:false,
        buttons: [{
          text: this.translate.instant('ADDPROD.EXIT'),
          handler: () => {
            this.DoCopyInCurrentPutOriginal();
            this.navCtrl.pop();
            //this.navCtrl.push(ListProdPage,{search:false,productToDelete:null});
            resolve();
          },
        }, {
          text: this.translate.instant('GLOBAL.REMAIN'),
          handler: () => {
            reject();
          }
        }],
      });
      confirm.present();
    })
  }

  /**
   * binda sul current i valori originali.
   * @constructor
     */
  private  DoCopyInCurrentPutOriginal(){
    this.currentProduct.id =  this.originalProduct.id;
    this.currentProduct.local_id =  this.originalProduct.local_id;
    this.currentProduct.warehouse_id =  this.originalProduct.warehouse_id;
    this.currentProduct.type_id =  this.originalProduct.type_id;
    this.currentProduct.qrcode =  this.originalProduct.qrcode;
    this.currentProduct.amount =  this.originalProduct.amount;
    this.currentProduct.bestbeforedate =  this.originalProduct.bestbeforedate;
  }

  /**
   * Esegue una copia del prod
   * @constructor
   */
  private DoCopyInOriginalPutCurrent(){
    let id =  this.currentProduct.id;
    let local_id =  this.currentProduct.local_id;
    let warehouse_id =  this.currentProduct.warehouse_id;
    let type_id =  this.currentProduct.type_id;
    let qrcode =  this.currentProduct.qrcode;
    let amount =  this.currentProduct.amount;
    let bestbeforedate =  this.currentProduct.bestbeforedate;
    let insertdate =  this.currentProduct.insertdate;

    this.originalProduct = new Product(id, local_id, warehouse_id, type_id, qrcode, amount, bestbeforedate,insertdate)
  }

  /**
   * return a date formatted dddd dd MMMM yyyy. Es ven 8 Ago 2017
   * @param d data di unput
   * @returns {string} a date formatted dddd dd MMMM yyyy. Es ven 8 Ago 2017
   */
  public getDateFormatted(d:Date){
    if (isNaN(d.getTime())){
      return "";
    }
    return this.translate.instant('ADDPROD.INSERTED') + ":" + this.globalConf.getDateFormatted(d);
  }


  /**
   * sbianchetta l'amount in focus. apre la form modale per l'inserimento della quantità. sulla dismiss recupera il valore
   */
  private amountFocusEv(){
    this.currentProduct.amount=null;

    let modal = this.modalCtrl.create(Quantity,{
     });
    modal.onDidDismiss((value) => {
      if (value != null){
        this.defaultamount = "";
        this.productEdited();
        this.checkCalendarOpened();
      }else{
        this.defaultamount = "__";
      }
      this.currentProduct.amount = value;
    });
    modal.present();
  }
}
