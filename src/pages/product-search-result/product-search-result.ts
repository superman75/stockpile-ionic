import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Product} from "../../model/product";
import {DataManagerProvider} from "../../providers/data-manager/data-manager";
import {AngularFireDatabase} from "angularfire2/database";

/**
 * Generated class for the ProductSearchResultPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-search-result',
  templateUrl: 'product-search-result.html',
})
export class ProductSearchResultPage {

    products : any;
    warehouses : any;
    types : any;
    categories : any;
  colorProductOk:string="#E6D690";
  colorProductNok:string="#EAE6CA";
  product:Product;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private afDB : AngularFireDatabase,
              public dataManager:DataManagerProvider) {

    this.product =  navParams.get('product');
    if (this.product == null){

    }else{

    }

  }

  getCategory(product:Product){
    return product.getCategory(this.dataManager.m_typeList);
  }

  /**
   * torna il nome del magazzino dato l'id
   * @param warehouseid
   */
  public getWarehouseName(warehouseid:string){
      this.afDB.object('/warehouses/'+warehouseid).valueChanges().subscribe(value=>{
          return value['name'];
      });

    // return this.dataManager.getWarehouse(warehouseid).name;
  }

  public getCategoryName(catid:string){
      this.afDB.object('/categories/'+catid).valueChanges().subscribe(value=>{
          return value['name'];
      });
    // return this.dataManager.getCategory(catid).name;
  }

  public getTypeName(typeid:string){
    this.afDB.object('/types/'+typeid).valueChanges().subscribe(value=>{
      return value['name'];
    });
    // return this.dataManager.getType(typeid).name;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductSearchResultPage');
  }

}
