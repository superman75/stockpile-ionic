import { Component } from '@angular/core';
import {NavController, NavParams, ModalController, AlertController, ToastController } from 'ionic-angular';
import {AddProdPage} from "../add-prod/add-prod";
import {ListProdPage} from "../list-prod/list-prod";
import {DataManagerProvider} from "../../providers/data-manager/data-manager";
import {QRScannerPage} from "../qrscanner/qrscanner";
import {QRScanner} from "@ionic-native/qr-scanner";
import {GlobalConfProvider} from "../../providers/global-conf/global-conf";
import {Vibration} from "@ionic-native/vibration";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import {LoginPage} from "../login/login";
import {TranslateService} from "@ngx-translate/core";
import {AngularFireDatabase} from "angularfire2/database";
/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  user: firebase.User;
  products : any;
  userId : any;
  constructor(public nav: NavController,
              public navParams: NavParams,
              private dataManager:DataManagerProvider,
              private globalConf:GlobalConfProvider,
              public modalCtrl: ModalController,
              public qrScanner: QRScanner,
              private alertCtrl: AlertController,
              private fireAuth: AngularFireAuth,
              private toastCtrl: ToastController,
              public translate: TranslateService,
              private afDB : AngularFireDatabase,
              private afAuth : AngularFireAuth,
              private vibration: Vibration) {
      afAuth.authState.take(1).subscribe( data=>{
          this.userId = data.uid;
      });
  }


  openAddProd(){
    this.nav.push(AddProdPage);
  }

  openListaProd(){
    this.nav.push(ListProdPage);
  }

  openSearchProd(){
      this.afDB.object('/products').valueChanges().subscribe(value=>{
          if(!value){
              alert(this.translate.instant('PROD_SEARCH_RES.NO_PRODUCTS'));
          } else {
              let modal = this.modalCtrl.create(QRScannerPage);
              window.document.querySelector('page-home').classList.add('transparent-body');
              modal.onDidDismiss((qrcode) => {
                window.document.querySelector('page-home').classList.remove('transparent-body');
                  let p ;
                  this.products = this.afDB.list('products',ref=>ref.orderByChild('user_id').equalTo(this.user.uid)).valueChanges();
                  this.products.subscribe(snapShots=>{

                      snapShots.forEach(snapShot=>{
                          if(snapShot.qrcode===qrcode){
                              p = snapShot.id;
                          }
                      });
                      if(p!=null){
                          //ritolgo la trasparenza appena va bene
                          this.qrScanner.disableLight();
                          //apro il prodotto in edit, con i pulsanti, scarica salva nulla
                          this.nav.push(AddProdPage,{prodSelected:p,fromSearch:true});
                      }else{
                          if (qrcode!=null){
                              //vibrazione e continuo ricerca
                              this.vibration.vibrate(1000);
                              this.openSearchProd();
                          }
                      }
                  });
              });
              modal.present();
          }
      })

  }

  openLogout(){
      let alert = this.alertCtrl.create({
          title: this.translate.instant('LOGOUT.TITLE'),
          message: this.translate.instant('LOGOUT.ALERT_CONFIRM_MSG'),
          buttons: [
              {
                  text: this.translate.instant('GLOBAL.NO'),
                  role: 'cancel',
                  handler: () => {
                      console.log('Cancel clicked');
                  }
              },
              {
                  text: this.translate.instant('LOGOUT.ALERT_CONFIRM'),
                  handler: () => {
                      this.signOut();
                  }
              }
          ]
      });
      alert.present();
  }
    signOut(){

        this.fireAuth.auth.signOut()
            .then(res =>{
                console.log(res);
                this.nav.setRoot(LoginPage);
            })
            .catch(err =>{
                console.log(err)
                this.toastCtrl.create({
                    message: 'An error has occurred',
                    duration: 2000
                }).present();
            })
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');

      this.afDB.list('products',ref=>ref.orderByChild('user_id').equalTo(this.userId)).valueChanges()
          .subscribe(snapshots=>{
              snapshots.forEach(snapshot=>{
                  if(snapshot['bestbeforedate']<this.globalConf.dateconvert_to_yyyy_mm_dd_hh_mm_ss(new Date()) && !snapshot['isExpired']){
                      this.expired(snapshot['id'])
                  }
                  console.log(this.globalConf.is48hours(snapshot['bestbeforedate']));
              })
          })
  }
  expired(productId : string){
        this.afDB.object('products/'+productId).update({isExpired : true});
  }

}
