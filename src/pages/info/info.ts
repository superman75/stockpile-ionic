import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {GlobalConfProvider} from "../../providers/global-conf/global-conf";

/**
 * Generated class for the InfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  public clientVer;
  public storageVer;
  public serverApiVer;
  constructor(public navCtrl: NavController, public navParams: NavParams,public globalConf:GlobalConfProvider) {
  }

  ionViewDidLoad() {
    this.clientVer = this.globalConf.clientVer;
    this.storageVer = this.globalConf.storageVer;
    this.serverApiVer = this.globalConf.serverApiVer;
  }

}
