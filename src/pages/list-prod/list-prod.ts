import { Component } from '@angular/core';
import {NavController, NavParams, Platform, AlertController, LoadingController, ModalController} from 'ionic-angular';
import {DataManagerProvider} from "../../providers/data-manager/data-manager";
import {Product} from "../../model/product";
import {ProductSearchResultPage} from "../product-search-result/product-search-result";
import {AddProdPage} from "../add-prod/add-prod";
import {TranslateService} from "@ngx-translate/core";
import {GlobalConfProvider} from "../../providers/global-conf/global-conf";
import {Vibration} from "@ionic-native/vibration";
import {QRScannerPage} from "../qrscanner/qrscanner";
import {QRScanner} from "@ionic-native/qr-scanner";
import {TreaglesComboPopup} from "../../components/treaglesCombo/treaglesComboPopup";
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
export class ItemChoice {
  name;
  constructor(name){
    this.name=name;
  }
}


/**
 * Generated class for the ListProdPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-list-prod',
  templateUrl: 'list-prod.html',
})
export class ListProdPage {
  products : any;
  warehouses : any;
  types : any;
  categories : any;
  count_products : number = 0;
  userId : string;
  colorProduct:string="secondary"; //"#d6f5d6";
  productList;

  //QR Code
  results:any;
  loadingPanel;

  C_ALLstring:string="ALL";
  /**
   * filtro di default della pipe. Lo valorizzo in searchProduct
   */
  public myfiltyerQrCode:string=this.C_ALLstring;


  // all'inizio è rodinato perdata di scadenza
  toggleSortData:boolean=false;

  private selectedSortItem;
  private item1 = new ItemChoice(this.translate.instant("ITEM.ITEM1"));
  private item2 = new ItemChoice(this.translate.instant("ITEM.ITEM2"));
  private item3 = new ItemChoice(this.translate.instant("ITEM.ITEM3"));
  private item4 = new ItemChoice(this.translate.instant("ITEM.ITEM4"));
  private item5 = new ItemChoice(this.translate.instant("ITEM.ITEM5"));

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public afDB : AngularFireDatabase,
              private platform: Platform,
              private dataManager:DataManagerProvider,
              private alertCtrl:AlertController,
              private afAuth : AngularFireAuth,
              private translate:TranslateService,
              private loadingCtrl:LoadingController,
              private globalConf:GlobalConfProvider,
              private vibration: Vibration,
              public modalCtrl: ModalController,
              public qrScanner: QRScanner) {
      afAuth.authState.take(1).subscribe( data=>{
        this.userId = data.uid;
      });
    //se invece lo passo da listWares vado in edit
    /*DEPRECATO
    let search =  navParams.get('search');
    if (search!=null){
      if (search){
        this.searchProduct();
      }
    }*/

    this.selectedSortItem = this.item1;

    //se vengo dallo scarica dopo la ricerca allora elimino il prod, e gia mi ritrovo qui sulla lista
    let productToDelete =  navParams.get('productToDelete');
    if (productToDelete!=null){
        this.afDB.list('products').remove(productToDelete.id);
    }
  }

  ionViewDidLoad() {
    this.warehouses = this.afDB.list('warehouses').valueChanges();
    this.types = this.afDB.list('types').valueChanges();
    this.categories = this.afDB.list('categories').valueChanges();
    this.products = this.afDB.list('/products',
        ref=>ref.orderByChild('user_id').equalTo(this.userId)
    ).valueChanges();
    this.products.subscribe(items=>{
      this.count_products = items.length;
    });
  }
  /**
   * reset del filtro. torno tutti.
   */
  public clearfilterQrCode(){
    this.myfiltyerQrCode = this.C_ALLstring;
  }

  /**
   * chiamata dal bottone qrcode. se presente mostro risultati, altrimenti continuo a cercare
   */
  public searchProduct(){

    this.afDB.object('/products').valueChanges().subscribe(value=>{
        if(!value){
            alert(this.translate.instant('PROD_SEARCH_RES.NO_PRODUCTS'));
        } else {
            let modal = this.modalCtrl.create(QRScannerPage);
            window.document.querySelector('page-list-prod').classList.add('transparent-body');
            modal.onDidDismiss((qrcode) => {
                window.document.querySelector('page-list-prod').classList.remove('transparent-body');
                // let p = this.afDB.list('/products/',{
                //     ref => ref.orderByChild('qr_code') {
                //         orderByChild: 'qr_code',
                //         equalTo : qrcode
                //     }
                // });
                let p ;
                this.products.subscribe(snapShots=>{
                    snapShots.forEach(snapShot=>{
                        if(snapShot.qrcode==qrcode){
                            p = snapShot.id;
                        }
                    })
                    if(p!=null){
                        //ritolgo la trasparenza appena va bene
                        this.qrScanner.disableLight();
                        //apro il prodotto in edit, con i pulsanti, scarica salva nulla
                        this.navCtrl.push(AddProdPage,{prodSelected:p,fromSearch:true});
                    }else{
                        if (qrcode!=null){
                            //vibrazione e continuo ricerca
                            this.vibration.vibrate(1000);
                            this.searchProduct();
                        }
                    }
                    });
                // let p = this.globalConf.getProductQrCodeAlreadyPresent(this.dataManager.m_productList,qrcode);

            });
            modal.present();
        }
    })
  }




  /**
   * DEPRECATA
   * confornta il qrcode passato e cerca il prodotto che matcha
   * @param qrcode da confrontare
   */
  searchProductLocal(qrcode:string){
    this.products.subscribe(snapShots=>{
      snapShots.forEach(snapShot=>{
        if (snapShot.qrcode === qrcode){
            this.navCtrl.push(ProductSearchResultPage,{product:snapShot});
            return;
        }
      })
    });
    this.navCtrl.push(ProductSearchResultPage,{product:null});
  }

  /**
   * evento che si scatena al click. Apre la pagina per l'edit
   * @param e
   * @param product
     */
  public itemTapped(e,index : number){
    this.navCtrl.push(AddProdPage,{prodSelected:index});
  }

  /**
   *  Cancella il prodotto dal server
   * @param product prodotto da cancellare
     */
  public deleteProduct(product:any){
      let confirm = this.alertCtrl.create({
        title: this.translate.instant('LISTPROD.ALERT_DELETE_PROD'),
        message: this.translate.instant('LISTPROD.ALERT_DELETE_PROD_MSG') + " " + product.local_id ,
        buttons: [
          {
            text: 'No',
            handler: () => {
              //nothing
            }
          },
          {
            text: this.translate.instant('LISTPROD.ALERT_DELETE_PROD'),
            handler: () => {
                this.afDB.object('/products/'+product.id).remove();
            }
          }
        ]
      });
      confirm.present();
    }

  /**
   *
   */
  public ordinaDataScadenza(){
      let modal = this.modalCtrl.create(TreaglesComboPopup,{
        items: [this.item1, this.item2, this.item3, this.item4, this.item5],
        attrname: "Sort by ...",
        isNewToShow:false,
        isOkToShow:false,
        selectedItem: this.selectedSortItem,
        enableBackdropDismiss:true});
      modal.onDidDismiss((newSelectedItem,newValueCustom) => {
        //se il figlio passa Null non faccio nulla, altrimenti mando evento al papà di this
        if (newSelectedItem!=null){
          this.selectedSortItem = newSelectedItem;
          switch (this.selectedSortItem.name){
            case this.item1.name:
              this.products = this.products;
              break;
            case this.item2.name:
                this.products = this.products;
              break;
            case this.item3.name:
                this.products = this.products;
              break;
            case this.item4.name:
                this.products = this.products;
              break;
            case this.item5.name:
                this.products = this.products;
              break;
            default:
                this.products = this.afDB.list('products',ref=>ref.orderByChild('user_id').equalTo(this.userId)).valueChanges();
          }
        }
      });
      modal.present();

    /*
    if (!this.toggleSortData){

    }else{

    }
    this.toggleSortData = ! this.toggleSortData;
    */
  }

  //utilizzata per debug
  cleanDB(){
    //pulisco DB
    /*this.dataManager.cleandB().then((d)=>{
      //ricarico dati
      this.loadProductList();
    });*/
  }
}
