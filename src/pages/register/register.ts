import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController,AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AngularFireAuth } from 'angularfire2/auth';
import {LoginPage} from "../login/login";
import {HomePage} from "../home/home";
import {TranslateService} from "@ngx-translate/core";
import * as firebase from 'firebase';
/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  registerForm: FormGroup;
  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private fb: FormBuilder,
      private loadingCtrl: LoadingController,
      public translate: TranslateService,
      private toastCtrl: ToastController,
      private fireAuth: AngularFireAuth,
      private alertCtrl: AlertController
  ) {
      this.registerForm = this.buildRegisterForm();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
    private buildRegisterForm() {
        return this.fb.group({
            // 'email': ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
            // 'password': ['', Validators.required]
            email: ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        });
    }

    register(){
        let loading = this.loadingCtrl.create({
            content: 'Registering user ..',
            dismissOnPageChange: true
        });

        loading.present();

        let email = this.registerForm.get('email').value;
        let password = this.registerForm.get('password').value;

        this.fireAuth.auth.createUserWithEmailAndPassword(email, password)
        .then((res) => {
            console.log(res);
            loading.dismiss();
            // this.toastCtrl.create({
            //     message: 'you are welcome',
            //     duration: 2000
            // }).present();
            // this.navCtrl.setRoot(HomePage);
            let user = firebase.auth().currentUser;
            user.sendEmailVerification()
            .then(res => {
                console.log(res);
                this.toastCtrl.create({
                    message: "Email Verification is sent",
                    duration: 2000
                }).present();
            })
            .catch(err =>{
                console.log(err);
                this.toastCtrl.create({
                    message: 'An error has occurred',
                    duration: 2000
                })
            });
            this.goToLoginPage();
        })
        .catch(err =>{
            console.log(err);
            loading.dismiss();
            this.toastCtrl.create({
                message: err.message,
                duration: 2000
            }).present();
        })

    }
    goToLoginPage(){
        this.navCtrl.popToRoot();
    }
}
