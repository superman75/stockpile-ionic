import { Component } from '@angular/core';
import {NavController, ViewController} from 'ionic-angular';

import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import {GlobalConfProvider} from "../../providers/global-conf/global-conf";

@Component({
  selector: 'page-home',
  templateUrl: 'qrscanner.html'
})
export class QRScannerPage {

  constructor(public view: ViewController,
              public navCtrl: NavController,
              public qrScanner: QRScanner,
              private globalConf:GlobalConfProvider) {

    //metto un ritardo
    //this.sleep(100).then(() => {
      this.apriscanner();
    //})
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**
   * apro laluce
   */
  light(){
    if (this.globalConf.sunnyon){
      this.qrScanner.disableLight();
    }else{
      this.qrScanner.enableLight();
    }
    //toogle
    this.globalConf.sunnyon=!this.globalConf.sunnyon;
  }

  dismissView(){
    //this.qrScanner.disableLight();
    this.view.dismiss(null);
  }

  close(){
    this.view.dismiss(null);
  }

  apriscanner() {
    // Optionally request the permission early
   // this.qrScanner.prepare()

      this.qrScanner.prepare()
        .then((status: QRScannerStatus) => {
          if (status.authorized) {
            // camera permission was granted
            //alert('authorized');
            // start scanning
            if (this.globalConf.sunnyon) {
              this.qrScanner.enableLight();
            }
            let scanSub = this.qrScanner.scan().subscribe((text: string) => {
              console.log('Scanned something', text);
              // alert(text);
              //this.unlight();//toglo eventuale luce
              this.qrScanner.hide(); // hide camera preview
              scanSub.unsubscribe(); // stop scanning
              //passo al chiamante
              this.view.dismiss(text);
            });

            this.qrScanner.resumePreview();

            // show camera preview
            this.qrScanner.show()
              .then((data : QRScannerStatus)=> {
                //alert(data.showing);
              },err => {
                alert(err);
                this.dismissView();
              });

            // wait for user to scan something, then the observable callback will be called

          } else if (status.denied) {
            alert('denied');
            this.dismissView();
            // camera permission was permanently denied
            // you must use QRScanner.openSettings() method to guide the user to the settings page
            // then they can grant the permission from there
          } else {
            // permission was denied, but not permanently. You can ask for permission again at a later time.
            alert('else');
            this.dismissView();
          }
        })
        .catch((e: any) => {
          alert('Error is' + e);
          this.view.dismiss(null);
        });

  }

}
