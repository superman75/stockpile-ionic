export class Type {
  public id: string;
  public name:string;
  public category_id:string;
  constructor(id: string,name: string,category_id:string) {
    this.id = id;
    this.name = name;
    this.category_id = category_id;
  }
}

