import {Type} from "./type";
export class Product {
  public id:string;
  public local_id:string;
  public warehouse_id: string;
  public type_id:string;
  public qrcode:string;
  public amount:string;
  public bestbeforedate:string;
  public bestbeforedateDATE:Date;
  public insertdate:string;
  public insertdateDATE:Date;
  constructor(id: string,
              local_id: string,
              warehouse_id: string,
              type_id: string,
              qrcode:string,
              amount: string,
              bestbeforedate: string,
              insertdate:string) {
    this.id = id;
    this.local_id = local_id;
    this.warehouse_id = warehouse_id;
    this.type_id = type_id;
    this.qrcode = qrcode;
    this.amount = amount;
    this.bestbeforedate = bestbeforedate;
    this.insertdate = insertdate;
    
    //la data di inserimento è quella del db utc. quindi trasformo prima 
    this.insertdateDATE = new Date(this.insertdate + " UTC");

    this.bestbeforedateDATE = this.dateconvert_from_yyyy_mm_dd_hh_mm_ss_toDATE(bestbeforedate);
  }

  //prende una strina yyyy-MM-dd HH:mm:ss e la traforma in un oggetto dimtipo data
  private  dateconvert_from_yyyy_mm_dd_hh_mm_ss_toDATE(s:string):Date{
    var regexp:RegExp = /(\d{4})\-(\d{1,2})\-(\d{1,2}) (\d{2})\:(\d{2})\:(\d{2})/;
    var _result:Object = regexp.exec(s);

    return new Date(
      parseInt(_result[1]),
      parseInt(_result[2])-1,
      parseInt(_result[3]),
      parseInt(_result[4]),
      parseInt(_result[5]),
      parseInt(_result[6])
    );
  }

  /**
   * scorre la lista dei tipi passati in input e torna la categoria del tipo del prodotto
   * @param types
   * @returns {any}
     */
  public getCategory(types:Array<Type>){
    for (var i = 0; i < types.length; i++) {
      let type:Type = types[i];
      if (type.id == this.type_id){
        return type.category_id;
      }
    }
    return "";
  }


  public isExpired(){
    let today:Date = new Date();
    let curr:Date = this.bestbeforedateDATE;
    let yearToday = today.getFullYear();
    let yearCurr = curr.getFullYear();
    let monthToday = today.getMonth();
    let monthCurr = curr.getMonth();

    //check su anno e mese
    if (yearToday>yearCurr){
      return true;
    }
    if (yearToday<yearCurr){
      return false;
    }

    //caso stesso anno
    if (monthToday>monthCurr){
      return true;
    }
    if (monthToday<monthCurr){
      return false;
    }

    //stesso anno stesso mese. vedo il giorno
    let dayToday = parseInt(today.toDateString().substring(8,10));
    let dayCurr= parseInt(curr.toDateString().substring(8,10));
    if (dayToday>dayCurr){
      return true;
    }

    return false;

  }

}

