import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout'; // Don't forget this import, otherwise not gonna work
import {GlobalConfProvider} from "../global-conf/global-conf";

@Injectable()
export class AuthManagerProvider {

  constructor(public http: Http,
              public globalConf:GlobalConfProvider) {
    console.log('Hello AuthManagerProvider Provider');
  }

  login(username, password){
    //costruisco la stringa di variabili post
    let serverLoginUrl = this.globalConf.serverApiLoginUrl; //"http://treagles.it/oss-prezzi/api/V0.1/login";//
    let usernameencoded = encodeURIComponent(username);
    let passwordencoded = encodeURIComponent(password);
    let variables = "username="+usernameencoded+"&password="+passwordencoded;
    return new Promise(resolve => {
      this.http.get(serverLoginUrl+"?"+variables)
        .timeout(10000)
        .subscribe(data => {
            resolve(data);
          },
          err =>{
            resolve(err);
          });
    });
  }//fine login

}
