import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import {SqlProvider} from "../sql/sql";
import {Product} from "../../model/product";
import {GlobalConfProvider} from "../global-conf/global-conf";
import {User} from "../../model/user";
import {Type} from "../../model/type";
import {Warehouse} from "../../model/warehouse";
import {Category} from "../../model/category";

/*
  Generated class for the DataManagerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DataManagerProvider {

  USERNAME:string='username';
  PASSWORD:string='password';

  //modello
  //lista dei prodotti
  m_productList:Array<Product>=[];
  m_typeList:Array<Type>=[];
  m_warehouseList:Array<Warehouse>=[];
  m_categoryList:Array<Category>=[];

  constructor(private sql:SqlProvider,
              public http: Http,
              public globalConf:GlobalConfProvider) {

  }
  //salvo su db locale le credenziali
  saveUser(username:string,password:string):void{
    this.sql.set(this.USERNAME,username);
    this.sql.set(this.PASSWORD,password);
  }

  //legge le info di user salvate localmente. torna una promise, cioè lettura conclusa
  public readUser(){
    return new Promise(resolve => {
      this.sql.get(this.USERNAME).then(data=>{
        if (data!=null){
         let username = data;
          this.sql.get(this.PASSWORD).then(data=>{
            if (data!=null){
              this.globalConf.userLogged = new User(username,data);
            }
            resolve();
          });
        }
      });
    });
  }


  /**
   * Inserisce su db un nuovo Prodotto rilevato
   * @param item prodotto da aggiungere
   * @returns {Promise<any>} ritorna i dati tornati dal server o l'errore
   */
  insertProduct(product:Product){

    //return this.sql.insertProduct(item); //old GESTIRE SE DEVO memorizzare in locale
    return new Promise(resolve => {
      let serverUrl = this.globalConf.serverProductPostApiUrl; //"http://107.22.188.4/ossprezziwww/test.php"; //
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');

      //creo obj con le let in post
      let myobjectPrmPost = { local_id: product.local_id,
        warehouse_id:product.warehouse_id,
        type_id:product.type_id,
        qrcode:product.qrcode,
        amount: (parseFloat(product.amount)*100).toString(),
        bestbeforedate:product.bestbeforedate,
        username:this.globalConf.userLogged.username,
        password:this.globalConf.userLogged.password
      };

      //prima della post passo l'obj delle let ad una funzausiliaria che la formatta per bene
      this.http.post(serverUrl,this.globalConf.buildPostPrm(myobjectPrmPost),{headers:headers})
        .timeout(100000)
        .subscribe(data => {
          resolve(data);
        },err =>{
          resolve(err);
        });
    });

  }

  /**
   * cancella il proddotto a db
   * @param product da cancellare
   * @returns {Promise<T>}
     */
  deleteProduct(product:Product){
    //return this.sql.insertProduct(item); //old GESTIRE SE DEVO memorizzare in locale
    return new Promise(resolve => {
      let serverUrl = this.globalConf.serverProductPostApiUrl+"/"+product.id; //"http://107.22.188.4/ossprezziwww/test.php"; //
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
      //prima della post passo l'obj delle let ad una funzausiliaria che la formatta per bene
      this.http.delete(serverUrl,{params:{ username:this.globalConf.userLogged.username, password:this.globalConf.userLogged.password},headers:headers})
        .timeout(100000)
        .subscribe(data => {
          resolve(data);
        },err =>{
          resolve(err);
        });
    });
  }

  /**
   * Aggiorna il proddtto a db
   * @param product proddotto da aggiornare
   * @returns {Promise<T>}
     */
  updateProduct(product:Product){
    //return this.sql.insertProduct(item); //old GESTIRE SE DEVO memorizzare in locale
    return new Promise(resolve => {
      let serverUrl = this.globalConf.serverProductPostApiUrl+"/"+product.id; //"http://107.22.188.4/ossprezziwww/test.php"; //
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');

      //creo obj con le let in post
      let myobjectPrmPost = {id:product.id,
        local_id: product.local_id,
        warehouse_id:product.warehouse_id,
        type_id:product.type_id,
        qrcode:product.qrcode,
        amount:(parseFloat(product.amount)*100).toString(),
        bestbeforedate:product.bestbeforedate,
        username:this.globalConf.userLogged.username,
        password:this.globalConf.userLogged.password
      };

      //prima della post passo l'obj delle let ad una funzausiliaria che la formatta per bene
      this.http.post(serverUrl,this.globalConf.buildPostPrm(myobjectPrmPost),{headers:headers})
        .timeout(100000)
        .subscribe(data => {
          resolve(data);
        },err =>{
          resolve(err);
        });
    });

  }


  /**
   * Chiede al server la configurazione. Array di input +lista prodotti dell'utente
   * @returns {Promise<T>}
     */
  public loadServerConfig():Promise<string>{
    //3. popola le tabelle in locale chiamando le api server. Quando fatto nella promise torna il risultato.
    // don't have the data yet
    return new Promise(resolve => {
      let serverUrl = this.globalConf.serverConfigApiUrl;
      let usernamePassQueryString =  this.globalConf.getQueryStringUserNamePass();
      this.http.get(serverUrl+"?"+usernamePassQueryString)
        .timeout(10000)
        .map(res => res.json())
        .subscribe(data => {
            //ripulisco il modello
            this.clearModel();
            this.loadModelProductList(data);
            resolve('end');
          },
          //di solito se va qui è perchè non c'è la rete
          err =>{
            resolve(err);
          });
    });
  }//fine carica conf

  /**
   * Esegue reset del modello
   */
  private clearModel(){
    this.m_productList = [];
  }

  /**
   * carica il modello che rappresenta la lista dei prodotti salvatio a db
   * @param dataConf json di input letto dal server
     */
  private loadModelProductList(dataConf:any){
    //leggo i siongoli array
    let products = dataConf.products;//amount, bestbeforedate, local_id,qrcode,warehouse_id,type_id
    let warehouses = dataConf.warehouses;//id name
    let categories = dataConf.categories;//id name
    let types = dataConf.types;//category_id id name

    //popolo i modelli, che verranno poi bindati sui controli grafici

    this.populateWarehouseModel(warehouses)
    this.populateTypesModel(types)
    this.populateCategoriesModel(categories)
    this.populateProductsModel(products);
  }

  /**
   * popola il modello this.m_warehouseList
   * @param warehouses array di magazzini letti dal server
     */
  private populateWarehouseModel(warehouses){
    this.m_warehouseList=[];
    for (let i = 0; i < warehouses.length; i++) {
      let warehouse = warehouses[i];
      if (warehouse.id != null && warehouse.name!=null ){
        //inserisco sul modello visto che sto nel ciclo
        this.m_warehouseList.push(new Warehouse(warehouse.id.toString(),warehouse.name));
      }
    }
  }

  /**
   * popola il modello this.m_typeList
   * @param types array dei tipi letto dal server
     */
  private populateTypesModel(types){
    this.m_typeList=[];
    for (let i = 0; i < types.length; i++) {
      let type = types[i];
      if (type.id != null && type.name!=null && type.category_id!=null){
        //inserisco sul modello visto che sto nel ciclo
        this.m_typeList.push(new Type(type.id.toString(),type.name,type.category_id.toString()));
      }
    }
  }

  private populateCategoriesModel(categories){
    this.m_categoryList=[];
    for (let i = 0; i < categories.length; i++) {
      let category = categories[i];
      if (category.id != null && category.name!=null ){
        //inserisco sul modello visto che sto nel ciclo
        this.m_categoryList.push(new Category(category.id.toString(),category.name));
      }
    }
  }

  //popola il modello con i prodotti
  private populateProductsModel(products){
    this.m_productList=[];
    for (let i = 0; i < products.length; i++) {
      let product = products[i];
      if (product.id != null && product.local_id != null && product.warehouse_id!=null &&  product.type_id!=null && product.amount!=null ){
          //inserisco sul modello visto che sto nel ciclo
          this.m_productList.push(new Product( product.id.toString(),
            product.local_id.toString(),
            product.warehouse_id.toString(),
            product.type_id.toString(),
            product.qrcode,
            (parseFloat(product.amount)/100).toString().replace(",","."),
            product.bestbeforedate,
            product.created_at));
      }
    }

   this.sortProductsOnDate();
  }

  /**
   * scorre la lista dei proddotti e cancella dal modello quello passato
   * @param productToDelete prodotto da eliminare dal modello
   */
  deleteProductFromModel(productToDelete:Product){
    this.m_productList.forEach((prod, index)=>{
      if(prod.id == productToDelete.id){
        //toglie dall'array bindato
        this.m_productList.splice(index,1);
      }
    });
  }

  /**
   * ordino per data recente
   */
  public sortProductsOnDate(){
    if (this.m_productList!=null){
      if (this.m_productList.length > 0){
        this.m_productList.sort(function(a:Product,b:Product){
          let c = new Date(a.bestbeforedateDATE);
          let d = new Date(b.bestbeforedateDATE);
          return c.getTime() - d.getTime();
        });
      }
    }
  }

  /**
   * ordino per data recente
   */
  public sortProductsOnDateInsert(){
    if (this.m_productList!=null){
      if (this.m_productList.length > 0){
        this.m_productList.sort(function(a:Product,b:Product){
          let c = new Date(a.insertdateDATE);
          let d = new Date(b.insertdateDATE);
          return c.getTime() - d.getTime();
        });
      }
    }
  }

  public sortProductsOnCategory(){
    let me  = this;
    if (this.m_productList!=null){
      if (this.m_productList.length > 0){
        this.m_productList.sort(function(a:Product,b:Product){
          if ( me.getCategory( a.getCategory( me.m_typeList ) ).name < me.getCategory( b.getCategory( me.m_typeList ) ).name )return -1;
          if ( me.getCategory( a.getCategory( me.m_typeList ) ).name > me.getCategory( b.getCategory( me.m_typeList ) ).name ) return 1;
          return 0;
        });
      }
    }
  }

  public sortProductsOnType(){
    let me  = this;
    if (this.m_productList!=null){
      if (this.m_productList.length > 0){
        this.m_productList.sort(function(a:Product,b:Product){
          if( me.getType(a.type_id).name < me.getType(b.type_id).name ) return -1;
          if( me.getType(a.type_id).name > me.getType(b.type_id).name ) return 1;
          return 0;
        });
      }
    }
  }

  public sortProductsOnAmount(){
    if (this.m_productList!=null){
      if (this.m_productList.length > 0){
        this.m_productList.sort(function(a:Product,b:Product){
          return parseFloat(a.amount) - parseFloat(b.amount);
        });
      }
    }
  }

  /**
   * torna il magazzino dato l'id
   * @param warehouseid
   */
  public getWarehouse(warehouseid:string){
    for (let i = 0; i < this.m_warehouseList.length; i++) {
      let w:Warehouse = this.m_warehouseList[i];
      if (w.id == warehouseid){
        return w;
      }
    }
  }



  /**
   * torna la categoria dato l'id
   * @param catid
   */
  public getCategory(catid:string){
    for (let i = 0; i < this.m_categoryList.length; i++) {
      let c:Category = this.m_categoryList[i];
      if (c.id == catid){
        return c;
      }
    }
  }
  /**
   * torna il nome del tipo dato l'id
   * @param typeid
   */
  public getType(typeid:string){
    for (let i = 0; i < this.m_typeList.length; i++) {
      let t:Type = this.m_typeList[i];
      if (t.id == typeid){
        return t;
      }
    }
  }


  /*
   public update(item:Product){
   return this.sql.updateProduct(item);
   }

   public remove(item:Product){
   return this.sql.removeProduct(item);
   }

   public getProducts(){
   return this.sql.getProducts();
   }

   public cleandB(){
   return this.sql.cleanDB();
   }
   */

}
