import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {GlobalConfProvider} from "../global-conf/global-conf";
import {Product} from "../../model/product";
import {Platform} from "ionic-angular";

/*
 Generated class for the SqlProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular DI.
 */
@Injectable()
export class SqlProvider {
  public database: SQLiteObject;
  STORAGE_VER:string="STORAGE_VER";

  public isCordova: boolean;


  constructor(private sqlite:SQLite,
              private globalConf:GlobalConfProvider,
              private platform: Platform) {
    let storageVer:string = this.globalConf.storageVer;

    if(this.platform.is('cordova')){
      this.isCordova =true;
      this.sqlite.create({name: "data.db", location: "default"}).then((db : SQLiteObject) => {
        this.database = db;

        this.tryInit();
        //1. leggo versione storage
        this.get(this.STORAGE_VER).then(data=>{
          if (data!=null){
            switch (data) {
              case "1.0.0":
                switch(storageVer){
                  case "1.0.1":
                  // this.porting_from_100_to_101();
                }
                break;

              case "1.0.1":
                //non fare nulla
                break;
            }

          }else{
            //la prima volta creo il db come ver 1.0.0
            //this.init_100();
          }

          //salvo ver a db
          this.set(this.STORAGE_VER,storageVer);
        });


      }, (error) => {
        console.log("ERROR: ", error);
      });
    }else{
      this.isCordova =false;
      console.log("Apri la app sun un device per usare sqlLite");
    }

  }

  //la prima volta creo lo storage 100
  init_100(){

    //tabella dei prodotti
    let productTableCreate :string = `CREATE TABLE IF NOT EXISTS  products (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                warehouse_id TEXT,
                                type_id TEXT,
                                qrcode TEXT,
                                amount TEXT,
                                bestbeforedate TEXT
                                );`;
    try {
      return this.database.executeSql(productTableCreate, {});
    }catch(e){
      console.log("Error !");
    }
  }

  //DROPPA  e ricrea la tabella dei prodotti salvati in locale
  cleanDB(){
    //
    let sqlText;
    sqlText = `DROP TABLE products`;
    let that = this;
    return this.query(sqlText,{}).then((d)=>{
      return that.init_100();
    });
  }

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//********************************************GESTIONE PRODOTTI**************************************
//***************************************************************************************************
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //inserisce il prodotto
  insertProduct(item:Product){
    let sqlText;
    let values ;

    sqlText = "INSERT INTO products (warehouse_id,type_id,qrcode,amount,bestbeforedate) VALUES (?,?,?,?,?)";
    values = [item.warehouse_id || null ,
      item.type_id || null,
      item.qrcode || null,
      item.amount || null,
      item.bestbeforedate || null
    ]

    return this.query(sqlText,values);
  }

  public updateProduct(item:Product){
    let sqlText;
    let values ;
    sqlText = "UPDATE products SET (warehouse_id,type_id,qrcode,amount,bestbeforedate) = (?,?,?,?,?) where id = ? ; ";
    values = [item.warehouse_id || null ,
      item.type_id || null,
      item.qrcode || null,
      item.amount || null,
      item.bestbeforedate || null,
      item.local_id || null
    ]

    return this.query(sqlText,values);
  }
  public removeProduct(item:Product){
    let sqlText;
    let values ;
    sqlText = `delete from products where id = ? `;
    values = [item.local_id || null ]
    return this.query(sqlText,values);
  }

  public getProducts(){
    let sqlText;
    let values =[];
    sqlText = `select * from products`;
    return this.query(sqlText,values);
  }
//---------------------------------------------------------------------------------------------------
//********************************************FINE GESTIONE PRODOTTI*********************************
//***************************************************************************************************
//---------------------------------------------------------------------------------------------------



  //aggiungo per esempio una colonna
  porting_from_100_to_101(){
   /* let slqAlter = "ALTER TABLE products ADD COLUMN nuova TEXT ";

    this.sql.query(slqAlter,{}).then(data=> {
        //una volta creata la tab con la nuova colonna aggiorno al default
        let sqlUpd = "UPDATE products set nuova= 'nuovo valore'";
        this.sql.query(sqlUpd, {});
      }
    )*/
  }

  //crea la tabella delle chiavi se non esiste
  tryInit(){
    // Initialize the DB with our required tables
    this.database.executeSql('CREATE TABLE IF NOT EXISTS kv (key text primary key, value text)',{}).catch(err => {
      console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
    });
  }

  /**
   * Get the value in the database identified by the given key.
   * @param {string} key the key
   * @return {Promise} that resolves or rejects with an object of the form { tx: Transaction, res: Result (or err)}
   */
  public get(key:string):Promise<any> {
    if (this.isCordova) {
      return this.database.executeSql('select key, value from kv where key = ? limit 1', [key]).then(data => {
        if (data.rows.length > 0) {
          return data.rows.item(0).value;
        }else{
          return null;
        }
      });
    }else{
      return new Promise((resolve)=> {
        resolve("ok");
      });
    }

  }

  /**
   * Set the value in the database for the given key. Existing values will be overwritten.
   * @param {string} key the key
   * @param {string} value The value (as a string)
   * @return {Promise} that resolves or rejects with an object of the form { tx: Transaction, res: Result (or err)}
   */
  public set(key:string, value:string):Promise<any> {
    if (this.isCordova) {
      return this.database.executeSql('insert or replace into kv(key, value) values (?, ?)', [key, value]);
    }else{
      return new Promise((resolve)=> {
        resolve("ok");
      });
    }
  }

  query(tableName,values) {
    if (this.isCordova) {
      let that = this;
      return new Promise((resolve, reject)=> {
        that.database.executeSql(tableName, values).then((data)=> {
          resolve(data);
        }, (e)=> {
          reject(e);
        });
      });
    } else {
      return new Promise((resolve)=> {
        resolve("ok");
      });
    }
  }
}
