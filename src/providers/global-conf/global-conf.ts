import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {User} from "../../model/user";
import {Product} from "../../model/product";

/*
  Generated class for the GlobalConfProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class GlobalConfProvider {

  //variabili Globali
  public clientVer:string='0.3.1';
  public storageVer:string='1.0.0';
  public serverApiVer:string='1.0.0';

  private serverAddress:string= 'https://api.stockpile.mobi/'; //il default è produzione, quella del cliente
  private apiVer:string= 'api/V0.1/';
  //urls api
  public serverApiLoginUrl:string=  this.serverAddress + this.apiVer + 'login';
  public serverConfigApiUrl:string=  this.serverAddress + this.apiVer + 'config';
  public serverProductPostApiUrl:string=  this.serverAddress + this.apiVer + 'product';



  public userLogged:User;
  public currentPage:any;

  //globsle per capire se la luce sullo scanner èacesa o spenta
  public sunnyon:boolean=false;

  constructor(public http: Http) {
    console.log('Hello GlobalConfProvider Provider');
  }


  //torna la query string da passare come prm nelle chiamate http
  public getQueryStringUserNamePass():string {
    let usernameencoded = encodeURIComponent(this.userLogged.username);
    let passwordencoded = encodeURIComponent(this.userLogged.password);
    return "username="+usernameencoded+"&password="+passwordencoded;
  }

  //build the string to pass to the http POST
  public buildPostPrm(obj){
    var p = [];
    for (var key in obj) {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
  }

  public getCurrentePage(){
    return this.currentPage;
  }
  public setCurrentePage(currentPage:string):void {
    this.currentPage=currentPage;
  }

  //converte data js in YYYY-mm-dd HH:MM:SS
  public dateconvert_to_yyyy_mm_dd_hh_mm_ss(now) {
    let year = "" + now.getFullYear();
    let month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
    let day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
    let hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
    let minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
    let second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
  }
  public is48hours(s:string):boolean{
    let ss = this.dateconvert_from_yyyy_mm_dd_hh_mm_ss_toDATE(s).getTime();
    let today = new Date();
    let st = today.getTime();

    if (ss<st) return false;
    if (ss-172800000<st) return true;
    return false;
  }


  //prende una strina yyyy-MM-dd HH:mm:ss e la traforma in un oggetto dimtipo data
  public  dateconvert_from_yyyy_mm_dd_hh_mm_ss_toDATE(s:string):Date{
    var regexp:RegExp = /(\d{4})\-(\d{1,2})\-(\d{1,2}) (\d{2})\:(\d{2})\:(\d{2})/;
    var _result:Object = regexp.exec(s);

    return new Date(
      parseInt(_result[1]),
      parseInt(_result[2])-1,
      parseInt(_result[3]),
      parseInt(_result[4]),
      parseInt(_result[5]),
      parseInt(_result[6])
    );
  }


  /**
   * return a date formatted dddd dd MMMM yyyy. Es ven 8 Ago 2017
   * @param d data di unput
   * @returns {string} a date formatted dddd dd MMMM yyyy. Es ven 8 Ago 2017
   */
  public getDateFormatted(d:Date){
    //prima era così tramite pipe su html {{product.bestbeforedateDATE | date:'dd MMM yyyy'}}
    var days = ['Dom', 'Lun', 'Mart', 'Merc', 'Giovedì', 'Venerdì', 'Sabato'];
    var dayName = days[d.getDay()];

    var months=['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'] ;
    var monthName = months[d.getMonth()];

    var dateformatted:string = dayName + " " +  d.toDateString().substring(8,10) + " " + monthName + " " + d.getUTCFullYear();
    return dateformatted;

  }

  /**
   * torna true se nella lista dei prod è presenter quel qrcode passato
   * @param listProd
   * @param qrcode
     */
  getProductQrCodeAlreadyPresent(listProd:Array<Product>, qrcode:string):Product{

    if (qrcode==null) {
      return null;
    }
    //ciclo sui prodotti nel mio magazzino
    for(var i = 0 ; i < listProd.length ; i++) {
      let p:Product =  listProd[i];
      if (p.qrcode==qrcode){
        //ho trovato lo stesso qrcode
        return p;
      }
    }
    //se non esco prima significa che non ho trovato il prodotto nel mio magazino quindi torno false
    return null;
  }
}
