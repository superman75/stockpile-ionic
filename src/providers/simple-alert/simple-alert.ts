import { Injectable } from '@angular/core';
import {AlertController,Alert} from 'ionic-angular';

/*
  Generated class for the SimpleAlertProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SimpleAlertProvider {

  constructor(public alertCtrl: AlertController) {
    console.log('Hello SimpleAlertProvider Provider');
  }

  createAlert(title: string, message: string):Alert {
    return this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });
  }

}
