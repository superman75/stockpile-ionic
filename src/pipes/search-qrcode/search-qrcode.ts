import { Pipe, PipeTransform,Injectable } from '@angular/core';

/**
 * Generated class for the SearchQrcodePipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'searchQrcode',
})
@Injectable()
export class SearchQrcodePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(array: any[], filter: any):any {
    if (filter.qrcode == null || filter.qrcode === '' || array == null || filter.qrcode==='ALL') {
      return array;
    }

    return array.filter(item => {
      return item.qrcode === filter.qrcode;
    });
  }
}
