import {Component, Input,Output,EventEmitter} from "@angular/core";
import {ModalController} from "ionic-angular";
import {TreaglesComboPopup} from "./treaglesComboPopup";

@Component({
  selector: 'treagles-combo',
  templateUrl: 'treaglesCombo.html'
})

export class TreaglesCombo {

//nome attributo e lista di valori
  @Input() name:string;
  @Input() items:any[];
  @Input() selectedItem:any;
  @Input() nameicon:string;
  selectedValueCustom:string;
//evento che si scatena in output, per segnalare la selezione di un valore
  @Output() onValueChange:EventEmitter<any>;

  //valore bindato
  public value:string="";

  constructor(public modalCtrl: ModalController) {
    this.onValueChange = new EventEmitter();
  }

  //gestisce il click sulla combo. Apre il popup modale dove poter effettuare la scelta.
  //Nella onDidDismiss(...) è gestito il valore di ritorno. che può essere 1prm l'oggetto bindato, o 2a stringa
  //il nuovo valore. Se esco con cancel saranni (null,null) e quindi non faccio nulla
  onMyTreaglesComboClick(){
    let modal = this.modalCtrl.create(TreaglesComboPopup,{
      items: this.items,
      attrname: this.name,
      isNewToShow:false,
      isOkToShow:false,
      selectedItem: this.selectedItem,
      enableBackdropDismiss:true});
    modal.onDidDismiss((newSelectedItem,newValueCustom) => {
      //se il figlio passa Null non faccio nulla, altrimenti mando evento al papà di this
      if (newSelectedItem!=null){
        this.selectedItem = newSelectedItem;
        this.onValueChange.emit(this);
      }else if (newValueCustom!=null){
        //se è abilitata la text per immettere il nuovo valore allora valorizzo il campo apposito della combo, così fuori lo posso valutare
        this.selectedValueCustom = newValueCustom;
        this.onValueChange.emit(this);
      }
    });
    modal.present();
  }
}
