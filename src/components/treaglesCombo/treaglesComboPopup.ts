import {Component, ViewChild} from '@angular/core';
import {NavParams, ViewController, Content} from 'ionic-angular';

@Component({
  selector: 'treagles-combo-popup',
  templateUrl: 'treaglesComboPopup.html'
})
export class TreaglesComboPopup {

  items: any[];
  attrname:string;
  selectedItem:any;
  customText:string;
  isNewToShow:boolean=false;
  isOkToShow:boolean=false;
  @ViewChild(Content) content: Content;


  constructor( private view: ViewController,public params: NavParams) {
    this.attrname =  this.params.get('attrname');
    this.isNewToShow =  this.params.get('isNewToShow');
    this.isOkToShow =  this.params.get('isOkToShow');
    this.selectedItem =  this.params.get('selectedItem');
    this.customText =  "";
    this.initializeItems();
  }

  okEv(){
    //determino valore selezionato
    let newValue:any=null;
    let newValueCustom:string=null;
    if (this.customText!=""){
      newValueCustom = this.customText;
    }else{
      newValue = this.selectedItem;
    }
    //passo al chiamante
    this.view.dismiss(newValue,newValueCustom);
  }

  //su cancel passo stringa fissa NULL, così il padre non fa nulla
  cancelEv(){
    //chiudo e passo vuoto
    this.view.dismiss(null,null);
  }

  //inizializza la lista di items, prendendo dal prm passato dal papà
  initializeItems() {
    this.items =  this.params.get('items');
  }

  //alla selezione di una radio, sbianchetto il campo custom
  radioChange(itemClicked){
    //se c'è il tasto ok, sbianchetto il campo custom e non faccio nulla
    if (this.isOkToShow){
      this.customText="";
    }else{
      //chiudo e passo l'item cliccato al chiamante. che implementa onDidDismiss(). come valore custom passo null
      //ovviamente, perchè in questo caso, non c'è bottone ok, quindi chiudo ad ogni radiochange passando il valore selezionato
      this.view.dismiss(itemClicked,null);
    }
  }

  //filtra gli items, quando digito sulla text
  getItems(ev: any) {
    this.initializeItems();
    //frozo lo scroll totop per evitare che rimanga in una zona bianca. Il controllo non gestisce da solo lo scroll.
    this.scrollToTop();
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {

      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  customTrackBy(index, item) {
    return item.id;
  }

  scrollToTop() {
    this.content.scrollToTop();
  }

}
