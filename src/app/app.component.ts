import { Component, ViewChild } from '@angular/core';
import {Nav, Platform, AlertController, IonicApp, MenuController, ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {TranslateService} from "@ngx-translate/core";
import {LoginPage} from "../pages/login/login";
import {AddProdPage} from "../pages/add-prod/add-prod";
import {ListProdPage} from "../pages/list-prod/list-prod";
import {InfoPage} from "../pages/info/info";
import {GlobalConfProvider} from "../providers/global-conf/global-conf";

import { AppVersion } from '@ionic-native/app-version';
import { AppMinimize } from '@ionic-native/app-minimize';
import {HomePage} from "../pages/home/home";
import {SmartAudio} from "../providers/smart-audio/smart-audio";

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import {AngularFireDatabase} from "angularfire2/database";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public version:string;
  private isInBackBtn: boolean = false;
  rootPage: any ;

  pages: Array<{title: string, component: any,img:string}>;

  constructor(public platform: Platform, public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public alertCtrl: AlertController,
              public translate: TranslateService,
              private globalConf:GlobalConfProvider,
              private appVersion: AppVersion,
              public ionicApp:IonicApp,
              public menuCtrl:MenuController,
              private appMinimize: AppMinimize,
              private smartAudio: SmartAudio,
              private afDB : AngularFireDatabase,
              private fireAuth: AngularFireAuth) {

    this.initializeApp();

    //this.backgroundMode.enable();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.validateUserSessionOpen();
      //precarico suono
      this.smartAudio.preload('QrcodeSuccess', 'assets/sounds/qrcodefound.wav');
      this.smartAudio.preload('SameQrCode', 'assets/sounds/sameqrcode.wav');


      //Leggo e memorizzo la versione (quella che sta sul config.xml)
      if(this.platform.is('cordova')) {
        this.appVersion.getVersionNumber().then((version)=> {
          // alert(version);
          this.version = version;
          this.globalConf.clientVer = version;
        });
      }


      //******************************LOCALIZZAZIONE***************************************
      let userLang = navigator.language.split('-')[0]; // use navigator lang if available
      userLang = /(it|en)/gi.test(userLang) ? userLang : 'en';
      // this language will be used as a fallback when a translation isn't found in the current language
      this.translate.setDefaultLang('it');
      // the lang to use, if the lang isn't available, it will use the current loader to get them
      this.translate.use(userLang).subscribe(langObj => {
        //metto qui init dell'array di pagine perchè ocsì il servizio di traduzione è pronto
        this.pages = [
          { title: 'Home', component: HomePage ,img:'home'},
          { title: this.translate.instant('ADDPROD.TITLE'), component: AddProdPage ,img:'cloud-upload'},
          { title:  this.translate.instant('LISTPROD.TITLE'), component: ListProdPage ,img:'list-box'},
          { title: 'Info', component: InfoPage,img:'information-circle' }
        ];
      });
      //******************************FINE LOCALIZZAZIONE**********************************


      //******************************GESTIONE BACK BUTTON*********************************
      this.platform.registerBackButtonAction(() => {
        let nav = this.nav;
        if (!this.isInBackBtn) {
          this.isInBackBtn = true;

          //se ho aperto una finestra modale, la chiudo
          let activePortal = this.ionicApp._modalPortal.getActive();
          if (activePortal) {
            this.isInBackBtn = false;
            activePortal.dismiss();
            return;
          }

          //se il menù è aperto lo chiudo
          if (this.menuCtrl.isOpen()) {
            this.isInBackBtn = false;
            this.menuCtrl.close();
            return;
          }

          if (nav.canGoBack()) {
            //se può andare dietro faccio pop dal nav
            nav.pop();
            this.isInBackBtn = false;
          } else {
            //mando in background, senza chiudere
            this.isInBackBtn = false;
            this.appMinimize.minimize();
          }
        }
      });
      // **********FINE GESTIONE BACK BUTTON

    });
  }
    validateUserSessionOpen(){
        let user = firebase.auth().currentUser;
        console.log('user', user);
        if (user){
            if(user.emailVerified){
                this.rootPage = HomePage;
            }else{
                this.rootPage = LoginPage;
            }
        } else this.rootPage = LoginPage;

        // this.fireAuth.authState.subscribe((user)=>{
        //     console.log('user', user);
        //
        // })
    }
  //apre popup per la conferma della chiusura della app
  public confirmExitApp() {
    let confirm = this.alertCtrl.create({
      title: this.translate.instant('MAIN.ALERT_CONFIRM_EXIT'),
      message:  this.translate.instant('MAIN.ALERT_CONFIRM_EXIT_MSG'),
      enableBackdropDismiss:false,
      buttons: [
        {
          text: this.translate.instant('GLOBAL.NO'),
          handler: () => {
            //alert('rimango in app');
            this.isInBackBtn = false;
          }
        },
        {
          text: this.translate.instant('GLOBAL.EXIT'),
          handler: () => {
            this.exitAndcloseApp();
          }
        }
      ]
    });
    confirm.present();
  }

  //chiude la app
  private exitAndcloseApp(){
    this.platform.exitApp();
  }

  openPage(page) {
    //non posso mettere 2 pagine home
    if (page.title == "Home" && !this.nav.canGoBack() ){
      return;
    }

    //se premo sulla pagina, tranne logout, salvo in var globale, la pagina corrente, da dove vengo. così la rimetto
    if (page.title!=this.translate.instant('LOGOUT.TITLE')){
      this.globalConf.setCurrentePage(page.title);//set la lagina corrente
    }
    this.nav.push(page.component);
  }
}
