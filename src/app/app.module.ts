import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {SQLite} from "@ionic-native/sqlite";

import { AppVersion} from "@ionic-native/app-version";
import { AppMinimize } from '@ionic-native/app-minimize';
import { NativeStorage } from '@ionic-native/native-storage';
import { Vibration } from '@ionic-native/vibration';
import { QRScanner } from "@ionic-native/qr-scanner";

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule, Http } from '@angular/http';
import { AddProdPage } from "../pages/add-prod/add-prod";
import { ListProdPage } from "../pages/list-prod/list-prod";
import { LoginPage } from "../pages/login/login";
import { InfoPage } from "../pages/info/info";
import { GlobalConfProvider } from '../providers/global-conf/global-conf';
import { DataManagerProvider } from '../providers/data-manager/data-manager';
import { SqlProvider } from '../providers/sql/sql';
import { ProductSearchResultPage } from "../pages/product-search-result/product-search-result";

import { DatePickerModule } from 'datepicker-ionic2';
import { AuthManagerProvider } from '../providers/auth-manager/auth-manager';
import { SimpleAlertProvider } from '../providers/simple-alert/simple-alert';
import { SearchQrcodePipe } from '../pipes/search-qrcode/search-qrcode';
import {HomePage} from "../pages/home/home";
import {TreaglesCombo} from "../components/treaglesCombo/treaglesCombo";
import {TreaglesComboPopup} from "../components/treaglesCombo/treaglesComboPopup";
import {QRScannerPage} from "../pages/qrscanner/qrscanner";
import {SmartAudio} from '../providers/smart-audio/smart-audio';
import {NativeAudio} from "@ionic-native/native-audio";
import {BackgroundMode} from "@ionic-native/background-mode";
import {Quantity} from "../pages/add-prod/quantity/quantity";

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../config';
import {RegisterPage} from "../pages/register/register";
import { Firebase } from '@ionic-native/firebase';
import { FCM } from '@ionic-native/fcm';
import {AngularFireDatabaseModule} from "angularfire2/database";

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    AddProdPage,
    ListProdPage,
    LoginPage,
    RegisterPage,
    InfoPage,
    ProductSearchResultPage,
    SearchQrcodePipe,
    HomePage,
    TreaglesCombo,
    TreaglesComboPopup,
    QRScannerPage,
    Quantity
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    DatePickerModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig.fire),
    AngularFireDatabaseModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddProdPage,
    ListProdPage,
    LoginPage,
    RegisterPage,
    InfoPage,
    ProductSearchResultPage,
    HomePage,
    TreaglesCombo,
    TreaglesComboPopup,
    QRScannerPage,
    Quantity
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLite,
    GlobalConfProvider,
    AppVersion,
    AppMinimize,
    BackgroundMode,
    QRScanner,
    NativeStorage,
    DataManagerProvider,
    SqlProvider,
    AuthManagerProvider,
    SimpleAlertProvider,
    Vibration,
    QRScanner,
    SmartAudio,
    NativeAudio,
    AngularFireAuth,
    Firebase,
    FCM
  ]
})
export class AppModule {}
